// $Id$

function RefreshDesc(input) {
    div = document.getElementById("option_descr");
	var nid = Drupal.settings.descriptions[$(input).val()];
	div.innerHTML = nid;    
}
 
jQuery.fn.ucDescRefresh = function() {
  //for radios
  $('.add-to-cart .form-radios input').click(function(){
    RefreshDesc(this);
  });
  //for selects
  $('.add-to-cart select.form-select').change(function(){
    RefreshDesc(this);
  });
}

Drupal.behaviors.ucDesc = function() {
  $('.uc-aac-cart').ucDescRefresh();
}; 
