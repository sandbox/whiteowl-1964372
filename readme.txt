$Id$

Ubercart Option Descriptions 
Developed by WhiteOwl <rimma.owlet@gmail.com>     
http://www.protukan.ru	  

------------------------------------------------------------------------------- 
INSTALLATION
-------------------------------------------------------------------------------    
Place the content of this archive in sites/all/modules/ubercart/.
Navigate to admin/build/modules. 
Enable "Option Descriptions".
After installation you can add descriptions to your attribute options by navigating to:
/admin/store/attributes/X/options/X/edit.
	
-------------------------------------------------------------------------------
PERMISSIONS
-------------------------------------------------------------------------------      
view option description
              


